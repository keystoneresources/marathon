$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      mobileDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	mobileDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      mobileDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      mobileDrawerDisable();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      mobileDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      mobileDrawerDisable();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      mobileDrawerDisable();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){

	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      mobileDrawerDisable();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      mobileDrawerDisable();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
	// Stock popup
	
	var popup = $('#popup');
	$('#popup-trigger').click(function(e){
		e.preventDefault();
		popup.show().animate({
			width: '100%', 
			height: '100%',
			top: '0px',
			marginLeft: '-50%',
			opacity: 1
		}, 200, function(){ 
			$('.popup-inner').show();
			$('body').addClass('popup-open');
		});
	});
	
	$('#popup-close').click(function(e){
		e.preventDefault();
		$('.popup-inner').hide();
		$('body').removeClass('popup-open');
		popup.animate({
			width: '0px', 
			height: '0px',
			top: '50%',
			marginLeft: '0px',
			opacity: 0
		}, 200, function(){
			popup.hide();
		});
	});
	
	$('.columned-list').each(function(){
		$(this).columnizeList({columnAmount:3});  
	});
	
	//$('.columned-list').columnizeList({columnAmount:3});  
	
	if($('.columned-list .toggle').length){
		$('.columned-list ul').each(function(){
			$('li:gt(1)',this).hide();
		});
		$('.columned-list .toggle').addClass('toggle-more');
		$('.toggle').click(function(e){
			e.preventDefault();
			if($(this).hasClass('toggle-more')){
				$(this).removeClass('toggle-more').addClass('toggle-less');
				$(this).html("See Less");
				$(this).parent().find('ul').each(function(){
					$('li:gt(1)',this).slideDown('fast');
				});
	
			}else{
				$(this).removeClass('toggle-less').addClass('toggle-more');
				$(this).html("See More");
				$(this).parent().find('ul').each(function(){
					$('li:gt(1)',this).slideUp('fast');
				});
			} 
		});
	}
	
	
		
		// Search filters hide/show
		Array.prototype.indexOf = function(obj, start) {
    for (var i = (start || 0), j = this.length; i < j; i++) {
        if (this[i] === obj) { return i; }
    }
    return -1;
	}
	$('.search-filter ul').each(function(){
	    var count = 0;
	    $(this).closest('ul').find('li').each(function(){
	        count += 1;
	        if(count == 6){
	            $(this).closest('ul').append('<li class="filter-toggle"><a href="#">SHOW MORE</a></li>');            
	        };
	        if(count > 5){
	            $(this).hide();
	        }; 
	    });
	});
	$(document).delegate('.filter-toggle', 'click', function(){
	    var count = 0;
	    $(this).closest('ul').find('li').each(function(){
	        count += 1;
	        if(count > 5 && !$(this).hasClass('filter-toggle')){
	            $(this).toggle();   
	        };
	    });
	    $(this).children().text(function(i, text){ return text === 'SHOW MORE' ? 'SHOW LESS' : 'SHOW MORE'});
	    return false;
	});
 
	// Mobile Drawers
	
  function mobileDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.mobile-trigger-';
    var content = '#content_container';
    var drawer = '.mobile-drawer-';
    //var contentBorder = '#content_push';
    //$('.mobile-drawers').show();
    $('.mobile-drawers').show();
    $(drawer+'left').hide();
    $(drawer+'right').hide();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
	    $(drawer+'left').show();
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '300px'}, function(){
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
	    $(drawer+'right').show();
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-300px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//mobileDrawerEnable

  function mobileDrawerDisable(){
    var trigger = '.mobile-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.mobile-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


}); //End Document Ready